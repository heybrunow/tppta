//variaveis
console.log("Hello world!");
//alert("Passou aqui");

let nome = "Bruno";
let idade = 18;
let isProfessor = true;
let salario = 100.14

console.log(typeof (nome));
console.log(typeof (idade));
console.log(typeof (isProfessor));

//operadores aritimeticos + - / * 
console.log(2 + 2);

console.log("A soma de 2 + 2 é " + (2 + 2));

console.log(`A soma de 2 + 2 é ${2 + 2}`);
//funcao
//console.log("Alguma coisa")
//alert("lero lero")
//function nome (parametros) {escopo}
function escreverNoConsole() {
    console.log("Olá");
}
escreverNoConsole();

function podeDirigir(idade, cnh) {
    if (idade >= 18 && cnh == true) {
        console.log("Pode dirigir")
    } else {
        console.log("Nao pode dirigir")
    }
}

podeDirigir(19, true);
//no lugar de let tambem pode ser usado const 
let parOuImpar = (valor) => {
    let n = 5;
    if (valor % 2 == 0) {
        console.log("é par")
    } else {
        console.log("é impar")
    }

}
parOuImpar(2);

console.log(`A soma de 2 + 2 é ${2+2}`);

/*for (let x = 1; x <= 10; x++) {
    console.log("o valor de x é: " + x)
}*/

let numeros = [1, 3, 5 , 8, 12];

console.log(numeros);

console.log(numeros.length);

console.log(numeros[0]);
//ultima posicao do array
console.log(numeros.pop);

remover_ultimo = numeros.pop();
adicionar_ultimo = numeros.push(15);
console.log(numeros);

